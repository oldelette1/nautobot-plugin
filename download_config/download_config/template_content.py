# template_content
"""Added content to the device model view for config compliance."""
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Count, Q
from django.urls import reverse
from nautobot.dcim.models import Device
from nautobot.extras.plugins import PluginTemplateExtension
from download_config.models import TestConfig

# from nautobot_golden_config.models import ConfigCompliance, GoldenConfig
# from nautobot_golden_config.utilities.constant import CONFIG_FEATURES, ENABLE_COMPLIANCE
# from nautobot_golden_config.utilities.helper import get_device_to_settings_map


class ConfigDeviceCheck(PluginTemplateExtension):  # pylint: disable=abstract-method
    """Plugin extension class for config compliance."""

    model = "dcim.device"

    def get_device(self):
        """Get device object."""
        return self.context["object"]

    # def right_page(self):
    #     """Content to add to the configuration compliance."""
    #     # comp_obj = DownloadConfig.objects.filter(device=self.get_device()).values("rule__feature__name", "compliance")
    #     # extra_context = {
    #     #     "compliance": comp_obj,
    #     #     "device": self.get_device(),
    #     #     "template_type": "devicetab",
    #     # }
    #     return self.render(
    #         "nautobot_download_config/download_config.html",
    #         # extra_context=extra_context,
    #     )

    def detail_tabs(self):
        """Add a Configuration Compliance tab to the Device detail view if the Configuration Compliance associated to it."""
        try:
            return [
                {
                    "title": "Device Config",
                    "url": reverse("plugins:download_config:device_status_tab", kwargs={"pk": self.context["object"].pk}),
                    # "url": reverse("plugins:download_config:device_detail_tab", kwargs={"pk": self.context["object"].pk}),
                },
                # {
                #     "title": "Test",
                #     "url": reverse(
                #         "plugins:download_config:custom_view",
                #     ),
                # }
            ]
        except ObjectDoesNotExist:
            return []


class ConfigDeviceDetails(PluginTemplateExtension):  # pylint: disable=abstract-method
    """Plugin extension class for config compliance."""

    model = "dcim.device"

    # def right_page(self):
    #     """Content to add to the configuration compliance."""
    #     # return self.render(
    #     #     "nautobot_download_config/download_config.html",
    #     #     extra_context=extra_context,
    #     # )
    #     configs= DownloadConfig.objects.all()
    #     return self.render(
    #         'nautobot_download_config/download_config.html', 
    #         extra_context={'configs': configs,
    #     })



extensions = [ConfigDeviceDetails]
extensions.append(ConfigDeviceCheck)



template_extensions = extensions