# banner.py
from django.utils.html import format_html

from nautobot.extras.choices import BannerClassChoices
from nautobot.extras.plugins import PluginBanner

def banner(context, *args, **kwargs):
    """Greet the user, if logged in."""
    # Request parameters can be accessed via context.request
    if not context.request.user.is_authenticated:
        # No banner if the user isn't logged in
        return None
    else:
        return PluginBanner(
            # content=format_html("Hello, <strong>{}</strong>! 👋", context.request.user),
            content=format_html("Taiwan Semiconductor Manufacturing Company Limited 👋"),
            banner_class=BannerClassChoices.CLASS_SUCCESS,
        )
