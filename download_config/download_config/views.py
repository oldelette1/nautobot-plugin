# views.py
from django.shortcuts import render
from django.views.generic import View
from rest_framework.viewsets import ModelViewSet
# from .serializers import ConfigSerializer
from .models import TestConfig, DeviceConfig
from nautobot.dcim.models.devices import Device
from nautobot.apps.views import ObjectView
from .models import MacTable

class CustomConfigView(View):
    """Display a randomly-selected custom."""

    def get(self, request):
        # config = DownloadConfig.objects.order_by('?').first()
        configs = Device.objects.all()
        return render(request, 'nautobot_download_config/download_config.html', {
            'configs': configs,
        })

class DeviceDetailAppTab(ObjectView):
    """
    This view's template extends the device detail template,
    making it suitable to show as a tab on the device detail page.

    Views that are intended to be for an object detail tab's content rendering must
    always inherit from nautobot.apps.views.ObjectView.
    """

    queryset = Device.objects.all()
    template_name = "nautobot_download_config/devicetab.html"


class DeviceStatusTab(ObjectView):

    queryset = Device.objects.all()
    template_name = "nautobot_download_config/status.html"

class RunningConfigTab(ObjectView):

    queryset = Device.objects.all()
    template_name = "nautobot_download_config/running_config.html"

class StartupConfigTab(ObjectView):

    queryset = Device.objects.all()
    template_name = "nautobot_download_config/startup_config.html"


# class HistoryConfigTab(View):
#     """Display a history config"""

#     def get(self, request):
#         # config = DownloadConfig.objects.order_by('?').first()
#         configs = DeviceConfig.objects.all()
#         return render(request, "nautobot_download_config/history_config.html", {
#             'configs': configs,
#         })

class HistoryConfigTab(ObjectView):

    # queryset = DeviceConfig.objects.all()
    queryset = Device.objects.all()
    template_name = "nautobot_download_config/history_config.html"


# class ConfigViewSet(ModelViewSet):
#     """API viewset for interacting with DownloadConfig objects."""

#     # queryset = DownloadConfig.objects.all()
#     queryset = TestConfig.objects.order_by('id')[:10]
#     serializer_class = ConfigSerializer


# class CustomConfigView(View):
#     """Display a randomly-selected custom."""

#     def get(self, request):
#         # config = DownloadConfig.objects.order_by('?').first()
#         configs = DownloadConfig.objects.all()
#         return render(request, 'nautobot_download_config/download_config.html', {
#             'configs': configs,
#         })


# class MacTableView(ObjectView):

#     queryset = MacTable.objects.all()
#     template_name = "nautobot_download_config/mactable_list.html"