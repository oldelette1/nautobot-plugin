# urls.py
from django.urls import path
from rest_framework import routers
from . import views
from download_config.api.views import MacTableViewSet
from nautobot.apps.urls import NautobotUIViewSetRouter

urlpatterns = []

# router = routers.DefaultRouter()
# router.register("software", views.MacTableView)

# urlpatterns = router.urls

urlpatterns += [
    path('custom/', views.CustomConfigView.as_view(), name='custom_view'),
    # path('devicetab/', views.DeviceTabView.as_view(), name='devicetab'),
    path("devices/<uuid:pk>/", views.DeviceDetailAppTab.as_view(), name="device_detail_tab"),
    path("devices/<uuid:pk>/status/", views.DeviceStatusTab.as_view(), name="device_status_tab"),
    path("devices/<uuid:pk>/running_config/", views.RunningConfigTab.as_view(), name="device_running_tab"),
    path("devices/<uuid:pk>/startup_config/", views.StartupConfigTab.as_view(), name="device_startup_tab"),
    path("devices/<uuid:pk>/history_config/", views.HistoryConfigTab.as_view(), name="device_history_tab"),
    # path(
    #     "devices/<uuid:pk>/mactable/",
    #     MacTableViewSet.as_view(),
    #     name="device_mactable",
    # ),
]

# router = routers.DefaultRouter()
# router.register('config', views.ConfigViewSet)
# # router.register('devices/history_config', views.HistoryConfigTab)
# urlpatterns += router.urls

# urlpatterns = [
#     path("config", views.ConfigViewSet, name="config_overview"),
# ] + router.urls