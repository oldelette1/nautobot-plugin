# """API URLs for the MacTable Management plugin."""

from rest_framework import routers
from download_config.api.views import AnimalViewSet
from download_config.api.views import MacTableViewSet

router = routers.DefaultRouter()
router.register('animals', AnimalViewSet)
router.register('mactable', MacTableViewSet)
urlpatterns = router.urls