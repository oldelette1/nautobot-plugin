# # serializers.py
from rest_framework.serializers import ModelSerializer
from nautobot.apps.api import NautobotModelSerializer
# from ..models import TestConfig, DeviceConfig, MacTableModel, 
from ..models import Animal
from ..models import MacTable

class AnimalSerializer(ModelSerializer):
    """API serializer for interacting with Animal objects."""

    class Meta:
        model = Animal
        fields = ('id', 'name', 'sound')

class MacTableSerializer(NautobotModelSerializer):
    """REST API serializer for MacTable records."""

    class Meta:
        """Meta attributes."""

        model = MacTable
        fields = "__all__"

# class ConfigSerializer(ModelSerializer):
#     """API serializer for interacting with Animal objects."""

#     class Meta:
#         model = TestConfig
#         fields = ('id', 'name', 'running_config', 'startup_config')


# # class DeviceConfigSerializer(ModelSerializer):
# #     """API serializer for interacting with Animal objects."""

# #     class Meta:
# #         model = DeviceConfig
# #         fields = ('id', 'name', 'running_config', 'startup_config', 'backup_time')