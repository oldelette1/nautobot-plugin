# from .serializers import MacTableSerializer
from nautobot.apps.api import NautobotModelViewSet
from rest_framework.viewsets import ModelViewSet
from .serializers import AnimalSerializer
from ..models import Animal
from .serializers import MacTableSerializer
from ..models import MacTable
from nautobot.apps.views import ObjectView

class AnimalViewSet(ModelViewSet):
    """API viewset for interacting with Animal objects."""

    queryset = Animal.objects.all()
    serializer_class = AnimalSerializer


class MacTableViewSet(NautobotModelViewSet):
# class MacTableViewSet(ObjectView):
    """REST API viewset for MacTable records."""

    # queryset = MacTableModel.objects.prefetch_related("mac")
    queryset = MacTable.objects.all()
    serializer_class = MacTableSerializer
    # filterset_class = SoftwareLCMFilterSet