# models.py
from django.db import models
from nautobot.core.models.generics import OrganizationalModel, PrimaryModel
from nautobot.core.models import BaseModel
from django.core.validators import MaxValueValidator, MinValueValidator

class MacTable(PrimaryModel):
    """Model representing device mac address table record."""
    name = models.CharField(max_length=50)
    mac = models.CharField(max_length=50)
    interface = models.CharField(max_length=50)
    vlan_id = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(4095)])

    class Meta:
        """Meta attributes for SoftwareLCM."""

        verbose_name = "MacTable"
        unique_together = (
            "mac",
            "interface",
        )
    def __str__(self):
        """String representation of SoftwareLCM."""
        # return f"{self.mac} - {self.interface}"
        return self.name

class TestConfig(models.Model):
    """Base model for config."""

    name = models.CharField(max_length=50)
    running_config= models.CharField(max_length=50)
    startup_config= models.CharField(max_length=50)

    def __str__(self):
        return self.name

# class DeviceConfig(PrimaryModel):
class DeviceConfig(models.Model):
    """Base model for config."""

    name = models.CharField(max_length=50)
    date = models.CharField(max_length=50)
    running_config= models.CharField(max_length=50)
    startup_config= models.CharField(max_length=50)
    backup_time= models.CharField(max_length=50)

    # def __str__(self):
    #     return self.name

class Animal(PrimaryModel):
# class Animal(BaseModel):
    """Base model for animals."""

    name = models.CharField(max_length=50)
    sound = models.CharField(max_length=50)

    def __str__(self):
        return self.name