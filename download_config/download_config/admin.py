# admin.py
from django.contrib import admin
from nautobot.core.admin import NautobotModelAdmin

from .models import TestConfig, DeviceConfig
from .models import Animal
from .models import MacTable

@admin.register(MacTable)
class MacTableSAdmin(NautobotModelAdmin):
    list_display = ('mac', 'interface', 'vlan_id')

@admin.register(Animal)
class AnimalAdmin(NautobotModelAdmin):
    list_display = ('name', 'sound')

@admin.register(TestConfig)
class TestAdmin(admin.ModelAdmin):
    list_display = ('name', 'running_config', 'startup_config')

@admin.register(DeviceConfig)
class DeviceAdmin(admin.ModelAdmin):
    list_display = ('name', 'date','running_config', 'startup_config','backup_time')