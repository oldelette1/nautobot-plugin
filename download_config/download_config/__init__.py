from nautobot.extras.plugins import PluginConfig
from nautobot.core.signals import nautobot_database_ready
from nautobot.extras.plugins import NautobotAppConfig

class DownloadConfig(PluginConfig):
# class DownloadConfig(NautobotAppConfig):
    name = 'download_config'
    verbose_name = 'Download Config'
    description = 'An example plugin for development purposes'
    version = '0.1.0'
    author = 'mcleezs'
    author_email = 'mcleezs@tsmc.com'
    base_url = 'download_config'
    required_settings = []
    min_version= '2.0.2'
    max_version= '2.0.4'


    def ready(self):
        """Register custom signals."""
        from .signals import post_migrate_create_relationships  # pylint: disable=import-outside-toplevel

        nautobot_database_ready.connect(post_migrate_create_relationships, sender=self)

        super().ready()


config = DownloadConfig