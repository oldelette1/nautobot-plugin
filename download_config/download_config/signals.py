# signals.py

from nautobot.extras.choices import RelationshipTypeChoices

def post_migrate_create_relationships(sender, apps, **kwargs):
    """Create a Device-to-MacTable Relationship if it doesn't already exist."""

    # Use apps.get_model to look up Nautobot core models
    ContentType = apps.get_model("contenttypes", "ContentType")
    Device = apps.get_model("dcim", "Device")
    Device = apps.get_model("dcim", "Device")
    MacTable = sender.get_model("MacTable")
    _Relationship = apps.get_model("extras", "Relationship")

    # Ensure that the Relationship exists
    for relationship_dict in [
        {
            "label": "MacTable on Device",
            "key": "device_mactable",
            "type": RelationshipTypeChoices.TYPE_ONE_TO_MANY,
            "source_type": ContentType.objects.get_for_model(MacTable),
            "source_label": "Device that mac address table",
            "destination_type": ContentType.objects.get_for_model(Device),
            "destination_label": "Mac Address Table",
        },
    ]:
        _Relationship.objects.get_or_create(label=relationship_dict["label"], defaults=relationship_dict)
