# navigation.py
from nautobot.apps.ui import NavMenuImportButton,NavMenuAddButton, NavMenuGroup, NavMenuItem, NavMenuTab

items_operate = [
    NavMenuItem(
        link="plugins:download_config:mactable-list",
        name="Mac Table",
        permissions=[],
    ),
    NavMenuItem(
        link="plugins:download_config:custom_view",
        name="User Define",
        permissions=[],
    )
]

menu_items = (
    NavMenuTab(
        name="Download Config",
        weight=1000,
        groups=(
            NavMenuGroup(name="Manage", weight=100, items=tuple(items_operate)),
            # NavMenuGroup(name="Setup", weight=100, items=tuple(items_setup)),
        ),
    ),
)